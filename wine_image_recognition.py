#!/usr/bin/env python

import time
import sys
import requests
import pandas as pd
import matplotlib.pyplot as plt

from PIL import Image
from io import BytesIO
from sklearn.externals import joblib

class WineImageRecognition:
    '''
    Recognition of wine brand by the label image.
    '''
    def __init__(self, model_path='models/image_model.pkl',
            descr_path = 'data/descr_images.csv',
            drawing_dir='labels_images_results/',
            subscription_key = 'd79fe8e294c34578b86dc252b71bb3a6'):
        '''
        :param model_path: str
        :param drawing_path: str
        '''
        self.model = joblib.load(model_path)
        self.drawing_dir = drawing_dir
        self.descr_path = descr_path
        self.vision_base_url = 'https://northeurope.api.\
            cognitive.microsoft.com/vision/v2.0/'
        self.headers = {'Ocp-Apim-Subscription-Key': subscription_key,
                   'Content-Type': 'application/octet-stream'}

    def process_ocr(self, image_path):
        '''
        :param image_path: str
        :return: text and its polygons of the wine label
        
        '''
        recognize_text_url = self.vision_base_url + 'RecognizeText'
        image_data = open(image_path, "rb").read()
        params = {'mode' : 'Printed'}
        response = requests.post(recognize_text_url, headers=self.headers,
            params=params, data=image_data)
        response.raise_for_status()

        rec_url = response.headers["Operation-Location"]
        answer = requests.get(rec_url, headers=self.headers).json()
        while not "recognitionResult" in answer:
            answer = requests.get(rec_url, headers=self.headers).json()
            time.sleep(1)
            
        answer = answer["recognitionResult"]["lines"]
        polygons = [(line["boundingBox"], line["text"]) for line in answer]
        text = ' '.join([line["text"] for line in answer])
        return text, polygons
        
    def draw_ocr(self, image_path, polygons):
        '''
        :param image_path: str
        :param polygons: tuple
        '''
        plt.figure(figsize=(7, 8))
        image  = plt.imread(image_path)
        ax = plt.imshow(image)
        for polygon in polygons:
            vertices = [(polygon[0][i], polygon[0][i+1])
                for i in range(0,len(polygon[0]),2)]
            text = polygon[1]
            plt.text(vertices[0][0], vertices[0][1], text,
                fontsize=16, va="top", color='blue')
        plt.axis("off")
        plt.savefig(self.drawing_dir + 'example_ocr.jpg', 
            dpi=200, bbox_inches='tight')
        
    def draw_thumbnail(self, image_path):
        '''
        :param image_path: str
        '''
        image_data = open(image_path, "rb").read()
        thumbnail_url = self.vision_base_url + "generateThumbnail"
        params = {'width': '100', 'height': '100', 'smartCropping': 'true'}
        response = requests.post(thumbnail_url, headers=self.headers,
            params=params, data=image_data)
        response.raise_for_status()
        thumbnail = Image.open(BytesIO(response.content))
        thumbnail.save(self.drawing_dir + 'example_thumbnail.jpg',
            dpi=200, bbox_inches='tight')

    def recognize_mark(self, image_path):
        '''
        :param spectrum_path: to test data, str
        :param descr_path: to wine description file
        :return mark and its description: wine brand, str
        '''
        text, polygons = self.process_ocr(image_path)
        if self.drawing_dir:
            self.draw_ocr(image_path, polygons)
            self.draw_thumbnail(image_path)
        mark = self.model.predict([text])[0]
        descr = pd.read_csv(self.descr_path, index_col='wine')['descr']
        return mark, descr.loc[mark]

if __name__ == '__main__':
    print(WineImageRecognition().recognize_mark(sys.argv[1]))
