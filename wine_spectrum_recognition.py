#!/usr/bin/env python

import sys
import pandas as pd
from sklearn.externals import joblib

class SpectrumModel:
    '''
    ML pretrained model for recognition mark of the wine by its spectrum.
    '''
    pass

class DescriptionTable:
    '''
    CSV file with wine descriptions.
    '''
    pass

class SpectrumData:
    '''
    CSV file with source spectra measurements.
    '''
    pass

class WineSpectrumRecognition:
    '''
    Recognition of wine brand by its spectrum values.
    '''
    def __init__(self, model_path='models/spectrum_model.pkl',
            descr_path='data/descr.csv'):
        '''
        :param model_path: str
        '''
        self.model = joblib.load(model_path)
        self.descr_path = descr_path

    def process_spectrums(self, spectrums):
        '''
        :param spectrums: raw data frame from device
        :return: clean data
        
        '''
        spectrums = spectrums.T.iloc[4:, 300:900].astype(int)
        spectrums.index = [int(s[8:]) for s in spectrums.index]
        spectrums.index.name = 'spectrum'
        return spectrums.values

    def recognize_mark(self, spectrum_path, descr_path='data/descr.csv'):
        '''
        :param spectrum_path: to test data, str
        :param descr_path: to wine description file
        :return mark and its description: wine brand, str
        '''
        spectrum = self.process_spectrums(pd.read_csv(spectrum_path))
        mark = self.model.predict(spectrum)[0]
        descr = pd.read_csv('data/descr.csv', index_col='wine')['descr']
        return mark, descr.loc[mark]

if __name__ == '__main__':
    print(WineSpectrumRecognition().recognize_mark(sys.argv[1]))
